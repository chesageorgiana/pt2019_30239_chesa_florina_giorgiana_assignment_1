/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vedere;

/**
 *
 * @author Apostol Bogdan
 */
import Model.*;
import Controller.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;


public class Interfata extends JFrame
{
    private static final String INITIAL_VALUE = "";
    private JPanel p=new JPanel();
    private JPanel panouControl1;
    private JPanel panouControl2;
    private JPanel panouControl3;
    private JFrame frame;
    private JLabel info1 = new JLabel(); 
   // private JLabel info2 = new JLabel();
    private JLabel info3 = new JLabel();
    private JLabel info4 = new JLabel();
    private JLabel info5 = new JLabel();
    private JButton buton1 =new JButton("OK");
    private JButton adunare = new JButton("Adunare");
    private JButton inmultire = new JButton("Inmultire");
    private JButton scadere = new JButton("Scadere");
    private JButton derivare = new JButton("Derivare");
    private JButton integrare = new JButton("Integrare");
    private JButton reset = new JButton("Reset");
     private JButton ce = new JButton("?");

// Code adding the component to the parent container - not shown here

    private JTextField polinom1 = new  JTextField("",20);
    private JTextField polinom2 = new  JTextField("",20);
    private JTextField rezultat = new  JTextField("",28);
    private JTextField afisp1 = new  JTextField("",20);
    private JTextField afisp2 = new  JTextField("",20);
    private Operatie sarc=new Operatie();
    private String s1,s2;
    private char s=' ';
    private char minus='-';
    private int grad1,grad2;
    private Polinom pol1;
    private Polinom pol2;
    //constructor of the class Interfata
         public Interfata(Polinom model1,Polinom model2){ //set up the logic !!!
             pol1=model1;
             pol2=model2;
                         
          this.setTitle("OPERATII CU POLINOAME");
          setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);         
          panouControl1 = new JPanel();
          panouControl2 = new JPanel();
          panouControl3 = new JPanel();
          p.setLayout(new BorderLayout());
          info4.setText(" P(x)= ");
          panouControl1.add(info4);
          panouControl1.add(polinom1);
          panouControl1.setPreferredSize(new Dimension(510,100));
          panouControl1.add(afisp1);
          panouControl1.add(info1);
          info5.setText("Q(x)= ");
          panouControl1.add(info5);
          panouControl1.add(polinom2);         
          panouControl1.add(afisp2);         
          panouControl1.add(buton1);
          panouControl2.add(inmultire);
          panouControl2.add(adunare);
          panouControl2.add(scadere);      
          panouControl2.add(derivare);
          panouControl2.add(integrare);
          panouControl2.add(reset);
          info3.setText("<html>"+"Rezultatul operatiei alese este : ");
          panouControl3.add(info3);
          rezultat.setEditable(false);
          panouControl3.add(rezultat); 
           panouControl1.add(ce);
          p.add(panouControl1, BorderLayout.NORTH);
          p.add(panouControl2, BorderLayout.CENTER);
          p.add(panouControl3, BorderLayout.SOUTH);
          this.setContentPane(p);
          this.pack();
         }
         //o metoda care realizeaza actiunea butonului1 dar atentie caci Listenerul
         //acestul buton se afla separat intr-o clasa din pachetul Controller datorita
         //modelului MVC
          public void actiunebuton1(Polinom a,Polinom b){
            
             pol1=a;
            pol2=b;
            s1=polinom1.getText();
            s2=polinom2.getText();
            String aux="";
            int j1=0,j2=0;
            grad1=0;
            grad2=0;
            int m=0;
     try {
           for(int i=0;i<s1.length();i++){              
           if (s1.charAt(i)!=s)
           { if (s1.charAt(i)==minus) m=1;
           else
           aux=aux+s1.charAt(i);
           }
           else{              
              //scriu ce vreau sa fac cu valoarea precedenta care e in aux
               int k=Integer.parseInt(aux);
               if (m==1){
               pol1.setCoef(j1, -k); m=0;}
               else
                    pol1.setCoef(j1, k);
               grad1++;
               j1++;
               aux="";
                        
               }
           }
                    } catch (Exception e) {
    // Print out the exception that occurred
    JOptionPane.showMessageDialog(frame, "Nu introduceti 2 spatii consecutive !!!");
}
           aux="";
            m=0;
           
            pol1.setDeg(grad1);
          //for(int i=0;i<pol1.getDeg();i++){  System.out.println(pol1.getCoef(i));}
         try {   
            for(int i=0;i<s2.length();i++){              
           if (s2.charAt(i)!=s)
           { if (s2.charAt(i)==minus) m=1;
           else
           aux=aux+s2.charAt(i);
           }
           else{              
              //scriu ce vreau sa fac cu valoarea precedenta care e in aux
               int k=Integer.parseInt(aux);
               if (m==1){
               pol2.setCoef(j2, -k); m=0;}
               else
                    pol2.setCoef(j2, k);
               grad2++;
               j2++;
               aux="";
                        
               }
           }
           
          
           pol2.setDeg(grad2);
             pol1.reverse();
          
             pol2.reverse();
              afisp1.setText(sarc.afisarepol(pol1));
             afisp2.setText(sarc.afisarepol(pol2));
           } catch (Exception e) {
    // Print out the exception that occurred
    JOptionPane.showMessageDialog(frame, "Nu introduceti 2 spatii consecutive !!!");
}
         
         }
           //actiunea butonului reset :reseteaza toate campurile cu 0
          public void reset() {
          rezultat.setText(INITIAL_VALUE);
          polinom1.setText(INITIAL_VALUE);
          polinom2.setText(INITIAL_VALUE);
          afisp1.setText(INITIAL_VALUE);
          afisp2.setText(INITIAL_VALUE);
          
          }
          //o metoda ce afiseaza rezultatul rez in jtext-ul rezultat
          public void afisare(String rez){
              rezultat.setText(rez);
          }
           public void ces(){
              
   JOptionPane.showMessageDialog(frame, "Mini HELP : Introduceti coeficienti la P(X) si Q(X) "
           + "cu observatia sa nu puneti 2 spatii consecutive intre acestia . \n       "
           + "   Abia apoi puteti selecta "
           + "ce operatie doriti sa faceti . Multumesc pentru intelegere !!!");
          }
          
          //actiunea butonului adunare
          public void adunare(Polinom a,Polinom b){
             pol1=a;
             pol2=b;
               String rez;
            rez=sarc.adunare(pol1, pol2);
            afisare(rez);
          }
          //actiunea butonului scadere
           public void scadere(Polinom a,Polinom b){
             pol1=a;
             pol2=b;
               String rez;
            rez=sarc.scadere(pol1, pol2);
            afisare(rez);
          }
           //actiunea butonului derivare
          public void derivare(Polinom a){
              pol1=a;
              afisare(sarc.derivare(pol1));
          }
          //actiunea butonului inmultire
          public void inmultire(Polinom a,Polinom b){
              pol1=a;
              pol2=b;
              afisare(sarc.inmultire(pol1,pol2));
          }
          //actiunea butonului integrare
             public void integrare(Polinom a){
              pol1=a;
             
              afisare(sarc.integrare(pol1));
          }
       
            /**
            *Mai jos sunt puse Listenerele pentur fiecare buton din Interfata grafica
            * a proiectului;
            */
          public void addListenerreset(ActionListener res){
              reset.addActionListener(res);
          }
           public void addListenerce(ActionListener ces){
              ce.addActionListener(ces);
          }
           
          public void addListenerbuton1(ActionListener b1){
             buton1.addActionListener(b1);
          }
        
          public void addListenerInmultire(ActionListener inm){
              inmultire.addActionListener(inm);
          }
          public void addListenerDerivare(ActionListener der){
              derivare.addActionListener(der);
          }
          public void addListenerAdunare(ActionListener adn){
              adunare.addActionListener(adn);
          }
          public void addListenerScadere(ActionListener scd){
              scadere.addActionListener(scd);
          }
           public void addListenerIntegrare(ActionListener ing){
              integrare.addActionListener(ing);
          }

}
