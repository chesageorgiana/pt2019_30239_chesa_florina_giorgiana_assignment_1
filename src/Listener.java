/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author Bogdan
 */
import Model.Polinom;
import Vedere.Interfata;
import java.awt.event.*;

public class Listener {
    //... The Control needs to interact with both the Model and Vedere. 
    private Polinom p1_model;
    private Polinom p2_model;
    private Interfata  i_view;
    
   
    
    public Listener(Polinom model1,Polinom model2, Interfata view) {
        p1_model = model1;
        p2_model = model2;
        i_view  = view;
        
        //... Add listeners to the view.
        view.addListenerbuton1(new addListenerbuton1());     
        view.addListenerInmultire(new addListenerInmultire());
        view.addListenerDerivare(new addListenerDerivare());
        view.addListenerAdunare(new addListenerAdunare());
        view.addListenerScadere(new addListenerScadere());
        view.addListenerIntegrare(new addListenerIntegrare());
        view.addListenerreset(new addListenerreset());
         view.addListenerce(new addListenerce());
        
    }
    
  
     class addListenerreset implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        i_view.reset();
        }
      }
      class addListenerce implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        i_view.ces();
        }
      }
     
    
    class addListenerbuton1 implements ActionListener {
      public void actionPerformed(ActionEvent e) {
       i_view.actiunebuton1(p1_model,p2_model);
       }
      }
   
        class addListenerInmultire implements ActionListener {
        public void actionPerformed(ActionEvent e) {
       i_view.inmultire(p1_model,p2_model);
        }
      }
         class addListenerDerivare implements ActionListener {
        public void actionPerformed(ActionEvent e) {
       i_view.derivare(p1_model);
        }
      }
          class addListenerAdunare implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            i_view.adunare(p1_model,p2_model);
    
        }
      }
           class addListenerScadere implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        i_view.scadere(p1_model,p2_model);
        }
      }
           class addListenerIntegrare implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        i_view.integrare(p1_model);
        }
      }
         
}
