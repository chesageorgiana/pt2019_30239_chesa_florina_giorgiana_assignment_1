/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author Bogdan
 */
import Model.*;
import java.util.ArrayList;
/**
 * In aceasta clasa se efectueaza operatiile asupra polinoamelor precum si
 * constructia polinomului ca si sir datorita metodei toString
 */
public class Operatie {
    public Operatie(){
        super();
    }
    //aduna si afiseaza un sir care are ca rezultat a+b
    public String adunare(Polinom a,Polinom b){
        Polinom c = new Polinom();
        c.setDeg(Math.max(a.getDeg(), b.getDeg()));
        Float w=new Float(0);
        ArrayList<Float> aux1 = new ArrayList<Float>();
        ArrayList<Float> aux2 = new ArrayList<Float>();
        for (int i = 0; i < c.getDeg(); i++)
        {aux1.add(i,w);
        aux2.add(i,w);
        }
        for (int i = 0; i < a.getDeg(); i++) aux1.add(i,a.getCoef(i));
        for (int i = 0; i < b.getDeg(); i++)  aux2.add(i,b.getCoef(i));
        for (int i = 0; i < c.getDeg(); i++)  c.setCoef(i,aux1.get(i)+aux2.get(i));
        return toString(c.getDeg()-1,c.getintCoef());
    }
     //scade si afiseaza un sir care are ca rezultat a-b
      public String scadere(Polinom a,Polinom b){
        Polinom c = new Polinom();
        c.setDeg(Math.max(a.getDeg(), b.getDeg()));
        Float w=new Float(0);
        ArrayList<Float> aux1 = new ArrayList<Float>();
        ArrayList<Float> aux2 = new ArrayList<Float>();
        for (int i = 0; i < c.getDeg(); i++)
        {aux1.add(i,w);
        aux2.add(i,w);
        }
        for (int i = 0; i < a.getDeg(); i++) aux1.add(i,a.getCoef(i));
        for (int i = 0; i < b.getDeg(); i++)  aux2.add(i,-b.getCoef(i));
        for (int i = 0; i < c.getDeg(); i++)  c.setCoef(i,aux1.get(i)+aux2.get(i));
        return toString(c.getDeg()-1,c.getintCoef());
    }
    
    public String afisarepol(Polinom a){
        
        return toString(a.getDeg()-1,a.getintCoef());
    }
    //inmultirea polinoamelor a si b si este returnat sirul corespunzator
     public String inmultire(Polinom a,Polinom b) {
   
        Polinom c=new Polinom();
        int grad=a.getDeg()+b.getDeg();
        c.setDeg(grad);
        Float w=new Float(0);
        float[] f=new float[50];
        ArrayList<Float> coef = new ArrayList<Float>();
        for (int i = 0; i <= c.getDeg(); i++) coef.add(i,w);
           for (int i = 0; i <= a.getDeg()-1; i++)
           for (int j = 0; j <= b.getDeg()-1; j++){
               coef.set(i+j,coef.get(i+j)+(a.getCoef(i) * b.getCoef(j)));
             //  f[i+j]+=a.getCoef(i) * b.getCoef(j);
          // c.setCoef(i+j,(c.getCoef(i+j)+(a.getCoef(i) * b.getCoef(j))));
           }
           for (int i = 0; i <= c.getDeg(); i++)
               c.setCoef(i,coef.get(i));
        return toString(c.getDeg()-2,c.getintCoef());
    }
    //se deriveaza doar polinomul a
      public String derivare(Polinom a) {
         int deg=a.getDeg()-1;
        if (deg == 0) return "0";
        Polinom deriv = new Polinom();
        deriv.setDeg(deg-1);
        for (int i = 0; i < deg; i++)
            deriv.setCoef(i,(i+1)*a.getCoef(i+1));
           
        return toString(deriv.getDeg(),deriv.getintCoef());
    }
    // o metoda ce returneaza integrarea polinomului a insa fara limite de integrare
 public String integrare(Polinom a) {
     Polinom integral= new Polinom ();
     integral.setDeg(a.getDeg());
     for (int i=0 ; i<= a.getDeg() ; i++){
         if (i==0) {
             integral.setCoef(i, 0);
         }
         else {
         integral.setCoef(i,a.getCoef(i-1)/i);
         }
         }
     return toString(integral.getDeg(),integral.getintCoef())+" +C";
 }
      
      
   //aceasta metoda translateaza cele doua notiuni de coeficient si de grad
  // intr-un sir corespunzator polinomului dat
    
    public String toString(int deg, ArrayList<Float> coef) {
               
           if (deg ==  0) return ""+ coef.get(0);
        if (deg ==  1) return coef.get(1) + "x + " + coef.get(0);
        String s = coef.get(deg) + "x^" + deg;
        for (int i = deg-1; i >= 0; i--) {
            if      (coef.get(i) == 0) continue;
            else if (coef.get(i)  > 0) s = s + " + " + ( coef.get(i));
            else if (coef.get(i)  < 0) s = s + " - " + (-coef.get(i));
            if      (i == 1) s = s + "x";
            else if (i >  1) s = s + "x^" + i;
        }
        
        
        
        return s;
    }
    
}
