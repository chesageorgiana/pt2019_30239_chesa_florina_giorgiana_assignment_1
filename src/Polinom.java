/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Bogdan
 */
import java.util.ArrayList;
import java.util.Collections;
/**
 * O clasa care contine coeficientii si gradul polinomului . Detine diferite 
 * metode de returnare respectiv modificare a acestor doua caracteristici ale 
 * polinomului.
 */
public class Polinom {
    private int deg;
    private ArrayList<Float> coef = new ArrayList<Float>();
    //constructor of the class Polinom
    public Polinom (){
        Reset();
    }
    //o metoda ce reseteaza polinomul cu coeficienti nuli si grad 0
    public void Reset(){
        this.deg=0;
        coef.clear();
    }
    //o metoda ce modifica gradul polinomului
    public void setDeg(int x){
        this.deg=x;
    }
    //o metoda ce incrementeaza gradul polinomului cu +1
    public void incDeg(int k){
        this.deg=this.deg+1;
    }
    //o metoda ce returneaza gradul polinomului
    public int getDeg(){
        return(deg);
    }
    //o metoda ce seteaza un anumit coeficient 'i' cu o valoare float 'c'
    public void setCoef(int i,float c){
           
        this.coef.add(i,c);
        
    }
    public void setc2(int i,float c){
        this.coef.set(i, c);
    }
     
    //o metoda ce returneaza coeficientul de pe pozitia i din arraylist
   public float getCoef(int i){
      return coef.get(i);
      
   }
   //o metoda ce returneaza tot arraylist-ul coef
   public  ArrayList<Float> getintCoef(){
   
       return this.coef;
   }
   //o metoda auxiliara de inversare a array-list-ului
   public void reverse(){
       Collections.reverse(this.coef);
   }
       
    }
 

