/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

/**
 *
 * @author Bogdan
 */

import Controller.*;
import Model.*;
import Vedere.*;
//clasa Main de testare a proiectului 
public class Testare {
    public static void main(String[] args){
        Polinom      model1      = new Polinom();
        Polinom      model2      = new Polinom();
        Interfata       view       = new Interfata(model1,model2);
        Listener controller = new Listener(model1 , model2 , view);
        
        view.setVisible(true);
        
    }
}
